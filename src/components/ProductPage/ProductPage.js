import './ProductPage.css';

export const ProductPage = ({ title, price, image, description, category }) => {
  return (
    <div className='product'>
      <div className='product__image'>
        <img src={image} alt='avatar' />
      </div>
      <div className='product__items'>
        <div className='product__label'>
          <h2 className='product__title'>{title}</h2>
          <span className='product__price'>$ {price}</span>
        </div>
        <div className='descriptin-wrapper'>
          <h3 className='description__title'>Description</h3>
          <span className='description'>{description}</span>
        </div>
        <div className='category-wrapper'>
          <h3 className='category__title'>Category:</h3>
          <span className='category'>{category}</span>
        </div>
      </div>
    </div>
  );
};
