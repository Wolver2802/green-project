import React from 'react';
import logo from '../../image/Logo.png';
import location from '../../image/Location.svg';
import email from '../../image/Message.svg';
import phoneNumber from '../../image/Calling.svg';
import './Footer.css';
import { Link } from 'react-router-dom';

/**
 * @author
 * @function Footer
 **/

export const Footer = (props) => {
  return (
    <footer className='footer'>
      <div className='logo-wrapper'>
        <Link to={'/'}>
          <img src={logo} alt='logo' />
        </Link>
      </div>
      <div className='location'>
        <img src={location} alt='location' className='footer__img' />
        <span className='footer__label'>
          70 West Buckingham Ave. Farmingdale, NY 11735
        </span>
      </div>
      <div className='email'>
        <img src={email} alt='email' className='footer__img' />
        <span className='footer__label'>contact@greenshop.com</span>
      </div>
      <div className='phone-number'>
        <img src={phoneNumber} alt='phone-number' className='footer__img' />
        <span className='footer__label'>+88 01911 717 490</span>
      </div>
    </footer>
  );
};
