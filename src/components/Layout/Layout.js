import { Outlet } from 'react-router';
import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import { Slider } from '../Slider/Slider';
import './Layout.css';

function Layout() {
  return (
    <div>
      <Header />
      <Slider />
      <div className='main__wrapper'>
        <Outlet />
      </div>
      <Footer />
    </div>
  );
}

export default Layout;
