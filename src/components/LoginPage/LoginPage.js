import React, { useEffect, useState } from 'react';
import { Button } from '../../utils/Button/Button';
import cl from './LoginPage.module.css';

/**
 * @author
 * @function LoginPage
 **/

export const LoginPage = ({ children, visible, setVisible }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailDirty, setEmailDirty] = useState(false);
  const [passwordDirty, setPasswordDirty] = useState(false);
  const [emailError, setEmailError] = useState(
    'Поле обязательно для заполнения'
  );
  const [passwordError, setPasswordError] = useState(
    'Поле обязательно для заполнения'
  );
  const [formValid, setFormValid] = useState(false);

  useEffect(() => {
    if (emailError || passwordError) {
      setFormValid(false);
    } else {
      setFormValid(true);
    }
  }, [emailError, passwordError]);
  const rootClasses = [cl.myModal];

  if (visible) {
    rootClasses.push(cl.active);
  }

  const blurHandler = (e) => {
    switch (e.target.name) {
      case 'email':
        setEmailDirty(true);
        break;

      case 'password':
        setPasswordDirty(true);
        break;
      default:
        break;
    }
  };

  const emailHandler = (e) => {
    setEmail(e.target.value);
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(e.target.value).toLowerCase())) {
      setEmailError('Email невалидный');
    } else {
      setEmailError('');
    }
  };

  const passwordHandler = (e) => {
    setPassword(e.target.value);
    if (e.target.value.length < 8) {
      setPasswordError('Пароль должен содержать как минимум 8 символов');
      if (!e.target.value) {
        setPasswordError('Поле обязательно для заполнения');
      }
    } else {
      setPasswordError('');
    }
  };

  const login = (event) => {
    event.preventDefault();
    console.log({
      email: email,
      password: password,
    });
    alert('Форма успешно отправлена! Данные смотри в консоли');
  };

  return (
    <div className={rootClasses.join(' ')} onClick={() => setVisible(false)}>
      <div className={cl.myModalContent} onClick={(e) => e.stopPropagation()}>
        <h2 className={cl.form__title}>Login</h2>
        <form onSubmit={login} className={cl.form}>
          <label className={cl.form__label}>
            Enter your username and password to login.
          </label>
          {emailDirty && emailError && (
            <div style={{ color: 'red' }}>{emailError}</div>
          )}
          <input
            value={email}
            onChange={(e) => emailHandler(e)}
            onBlur={(e) => blurHandler(e)}
            name='email'
            className={cl.form__input}
            type='text'
            placeholder='Enter your email'
          />
          {passwordDirty && passwordError && (
            <div style={{ color: 'red' }}>{passwordError}</div>
          )}
          <input
            value={password}
            onChange={(e) => passwordHandler(e)}
            onBlur={(e) => blurHandler(e)}
            name='password'
            className={cl.form__input}
            type='password'
            placeholder='Enter your password'
          />
          <Button disabled={!formValid}>Войти</Button>
        </form>
      </div>
    </div>
  );
};
