import { Route, Routes } from 'react-router-dom';
import './App.css';
import { ElectronicsPage } from './components/ElectronicsPage/ElectronicsPage';
import Layout from './components/Layout/Layout';
import ProductsLayout from './components/Layout/ProductsLayout';
import { Main } from './components/Main/Main';
import { ProductsPage } from './components/ProductsPage/ProductsPage';
import { MensClothingPage } from './components/MensClothingPage/MensClothingPage';
import { JeweleryPage } from './components/JeweleryPage/JeweleryPage';
import { WomenClothingPage } from './components/WomenClothingPage/WomenClothingPage';

function App() {
  return (
    <div className='container'>
      <Routes>
        <Route path={'/'} element={<Layout />}>
          <Route index element={<Main />} />
          <Route path={'electronics'} element={<ProductsLayout />}>
            <Route index element={<ElectronicsPage />} />
            <Route path={':productId'} element={<ProductsPage />} />
          </Route>
          <Route path={'jewelery'} element={<ProductsLayout />}>
            <Route index element={<JeweleryPage />} />
            <Route path={':productId'} element={<ProductsPage />} />
          </Route>
          <Route path={"men's%20clothing"} element={<ProductsLayout />}>
            <Route index element={<MensClothingPage />} />
            <Route path={':productId'} element={<ProductsPage />} />
          </Route>
          <Route path={"women's%20clothing"} element={<ProductsLayout />}>
            <Route index element={<WomenClothingPage />} />
            <Route path={':productId'} element={<ProductsPage />} />
          </Route>
          <Route path={':productId'} element={<ProductsPage />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
