import React, { useState } from 'react';
import logo from '../../image/Logo.png';
import search from '../../image/search.svg';
import basket from '../../image/basket.svg';
import './Header.css';
import { Link } from 'react-router-dom';
import { LoginPage } from '../LoginPage/LoginPage';
import { Button } from '../../utils/Button/Button';

/**
 * @author
 * @function Header
 **/

export const Header = (props) => {
  const [modal, setModal] = useState(false);
  return (
    <header className='header'>
      <div className='logo-wrapper'>
        <Link to={'/'}>
          <img src={logo} alt='logo' />
        </Link>
      </div>
      <div className='menu'>
        <Link className='menu__item' to={'/'}>
          Home
        </Link>
        <Link className='menu__item' to={'electronics'}>
          Electronics
        </Link>
        <Link className='menu__item' to={'jewelery'}>
          Jewelery
        </Link>
        <Link className='menu__item' to={"men's clothing"}>
          Men's clothing
        </Link>
        <Link className='menu__item' to={"women's clothing"}>
          Women's clothing
        </Link>
      </div>
      <div className='items-wrapper'>
        <img src={search} alt='logo' className='items-wrapper__item' />
        <img src={basket} alt='logo' className='items-wrapper__item' />
        <Button onClick={() => setModal(true)}>Login</Button>
      </div>
      <LoginPage visible={modal} setVisible={setModal} />
    </header>
  );
};
